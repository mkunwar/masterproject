package ProgressiveTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import ProgressivePages.AddVehicle;
import ProgressivePages.Driver1;
import ProgressivePages.HomePage;
import ProgressivePages.PersonalInfo;
import ProgressivePages.Zipcode;

public class ProgressiveWebsiteTest1 {

	public static WebDriver driver = null;

	public static void main(String[] args) throws InterruptedException {

		// 1:invoke Browser
		invokeBrowser();

		// 2:perform actions
		// 3:close the browser generate report
		SignUpFlow();
	}

	public static void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.navigate().to("https://www.progressive.com");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		if (driver.getTitle().contains("Progessive")) {
			System.out.println(driver.getTitle() + "-------------------was launched");
		} else {
			System.out.println("Fail Browser was not invoked----------------------");
			System.exit(0);
		}
	}
	

	public static void SignUpFlow() throws InterruptedException {
		HomePage obj = new HomePage(driver);
		obj.clickAuto();

		Zipcode obj1 = new Zipcode(driver);
		obj1.enterZipcode("75038");
		obj1.clickAuto();

		PersonalInfo obj2 = new PersonalInfo(driver);
		obj2.enterFirstName("Sanjaya");
		obj2.enterMiddleName("");
		obj2.enterLastName("Kunwar");
		obj2.selectSuffix("Mr");
		obj2.enterDOB("04/10/1993");
		obj2.enterStreetNumberAndName("4031 N. Beltline Rd");
		obj2.enterApartment("");
		obj2.enterCity("");
		obj2.enterZipCode("");
		obj2.clickpostbox();
		obj2.clickQuote();

		AddVehicle obj3 = new AddVehicle(driver);
		obj3.clickVehicleYear("2018");
		obj3.clickVehicleMake("Dodge");
		obj3.clickVehicleModel("Challenger Hellcat");
		obj3.selectBodyType("2DR 6CYL");
		obj3.selectPrimaryUse("1");
		obj3.selectOwnorLease("2");
		obj3.selectVehicleOwnership("3");
		obj3.clickDone();
		obj3.clickContinue();

		Driver1 obj4 = new Driver1(driver);
		obj4.clickMaleGender("");
		obj4.selectMaritalStatus("Single");
		obj4.selectEducationBackground("7");
		obj4.selectEmploymentStatus("7");
		obj4.enterSocialSecurity("123-45-6789");
		obj4.selectPrimaryResidence("7");
		obj4.selectMovedin("No");
		obj4.selectUSLicensedStatus("Valid");
		obj4.selectHistoryofDriving("7");
		obj4.clickClaims("No");
		obj4.clickTickets("No");
		obj4.clickContinue();

	}

	public static void closeBrowser() {
		System.exit(0);
	}
}
