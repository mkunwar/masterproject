package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GoogleObject {
	WebDriver driver = null;
	WebElement element = null;

	By enterSearchButton = By.xpath("//input[@name='q']");
	By clickRadioButton = By.xpath("//input[@aria-label='Google Search']");
	
	public GoogleObject(WebDriver driver) {
		this.driver = driver;
	}

	public void enterSearchButton(String Search) {
		driver.findElement(enterSearchButton).sendKeys(Search);
	}

	public void clickRadioButton() {
		driver.findElement(clickRadioButton).click();
	}
}
