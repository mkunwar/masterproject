package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CerotidPageObjectModelWithMethods {

	
	//Class level variable
	WebDriver driver = null;
	
	//Creating By obj and  finding elements by xpath
	By selectCourse = By.xpath("//select[@id='classType']");
	By selectSession = By.xpath("//select[@id= 'sessionType']");
	By enterName   = By.xpath("//input[@data-validation-required-message=\"Please enter your name.\"])[1]");
	
	
	public CerotidPageObjectModelWithMethods(WebDriver driver) {
		this.driver = driver;
		
	}
	
			
	// Course Course
	public void selectCourse(String courseName) {
		// Creating new webElement Object
		WebElement element = driver.findElement(selectCourse);
		Select selectCourse = new Select(element);
		selectCourse.selectByVisibleText(courseName);
		
		
				
	}
	public void selectSession(String sessionName) {
		// Creating new webElement object
		WebElement element = driver.findElement(selectSession);
		Select selectSession = new Select(element);
		selectSession.selectByVisibleText(sessionName);
	}
	public void enterName(String name) {
		driver.findElement(enterName).sendKeys(name);
	
		
	 
	 
	}
	
public void inputAddress(String Adress) {

}
}
