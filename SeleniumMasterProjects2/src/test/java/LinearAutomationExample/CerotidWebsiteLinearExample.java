package LinearAutomationExample;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CerotidWebsiteLinearExample {
	static WebDriver driver;

	public static void main(String[] args) {
		// Step 1: invoke the Browser go to cerotid page
		invokeBrowser();
		// Step2: Navigate to Cerotid Website and fill form

		fillform();

		// Step3: Validate Sucess Message

	}

	private static void fillform() {
		// Select Course
		// Creating WebElement Obj finding the location of the element
		WebElement course = driver.findElement(By.xpath("//select[@id='classType']"));

		Select chooseCourse = new Select(course);
		String courseQaAutomation = "QA Automation";
		chooseCourse.selectByVisibleText(courseQaAutomation);
		// session selected
		WebElement session = driver.findElement(By.xpath("//select[@id= 'sessionType']"));
		Select chooseSession = new Select(session);
		String sessionType = "Upcoming Session";

		chooseSession.selectByVisibleText(sessionType);
		// entered FULLNAME

		WebElement fullName = driver.findElement(By.xpath("//input[@id='name']"));
		fullName.sendKeys("Sanjaya Kunwar");

		// ADDRESS INPUT
		WebElement address = driver.findElement(By.xpath("//input[@id='address']"));
		address.sendKeys("4031 N.Beltline Rd");
		// CITY INPUT
		WebElement city = driver.findElement(By.xpath("//input[@id='city']"));
		city.sendKeys("Irving");
		// STATE SELECTED
		WebElement state = driver.findElement(By.xpath("//select[@id='state']"));
		state.sendKeys("Tx");
		// ZIPCODE INPUT
		WebElement Zipcode = driver.findElement(By.xpath("//input[@id='zip']"));
		Zipcode.sendKeys("75038");
		// EMAIL INPUT
		WebElement Email = driver.findElement(By.xpath("//input[@id='email']"));
		Email.sendKeys("Sanjayakunwar333@gmail.com");
		// PHONE NUMBER INPUT
		WebElement phone = driver.findElement(By.xpath("//input[@id='phone']"));
		phone.sendKeys("4699124473");
		// VIASA STATUS SELECTED
		WebElement VisaStatus = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		VisaStatus.sendKeys("UScitizen");
		// MEDIA SOURCE SELECTED
		WebElement mediaSource = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		mediaSource.sendKeys("Friends/Family");
		// RELOCATE OPTION SELECTED
		WebElement relocate = driver.findElement(By.xpath("//input[@id='relocate']"));
		relocate.sendKeys("Yes");
		// EDUCATION COMPLETED
		WebElement educationDetails = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
		educationDetails.sendKeys("Bachelors Running");

	}

	private static void invokeBrowser() {
		// Set the system path

		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		// Chromedriver obj

		driver = new ChromeDriver();
		// Navigate to Cerotid Website

		driver.navigate().to("http://www.cerotid.com");
		System.out.println(driver.getTitle() + ".....................................was launched");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

}
