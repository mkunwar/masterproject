package LinearAutomationExample;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {

	public static void main(String[] args) {
		// creating webdriver obj
		// Setting the system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Creating new chromedriver obj

		WebDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");
		// maximize the browser
		driver.manage().window().maximize();
		
		//Created webElement obj for the txtBoxsearch
		WebElement txtBoxSearch = driver.findElement(By.xpath("//input[@name='q']"));
		
		txtBoxSearch.sendKeys("what is selenium");
		
		WebElement searchBtn = driver.findElement(By.xpath("//input[@name='btnK']"));
		searchBtn.sendKeys(Keys.RETURN);

	}

}
