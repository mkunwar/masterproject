package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import pages.CerotidWebsitePageElements;



//Created to test the page objects in CerotidWebsite
public class CerotidWebsitePageElementTest {

	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		invokeBrowserCerotidPage();
		fillform();
		

	}	
//Step 1:

	public static void invokeBrowserCerotidPage() throws InterruptedException {
		// Set the system path to point to the chromedriver.exe file
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Utilizing the driver global variable and creating a new chrome driver
		driver = new ChromeDriver();

		driver.navigate().to("http://www.cerotid.com");
		// maximize the window

		driver.manage().window().maximize();

		// Wait example
		TimeUnit.SECONDS.sleep(4);

		// Refreshing the screen
		driver.navigate().refresh();
		
		// Adding some condition
		String titleName = "Cerotid";
		
		if(driver.getTitle().contains(titleName)) {
		
		//Prints the title

		System.out.println(driver.getTitle()+ "..........................Was launched");
		
		}else {
			System.out.println("Expected title"+ titleName + "was not seen");
			System.out.println("Test Fail---- Ending Test");
			driver.quit();
			driver.close();
		}
		
	}
	
	//Step 2:
	public static void fillform() {
		Select chooseCourse = new Select(CerotidWebsitePageElements.selectCourse(driver));
		chooseCourse.selectByVisibleText("QA Automation");

	
	//Session Element
	Select chooseSession = new Select(CerotidWebsitePageElements.chooseSession(driver));
	chooseSession.selectByVisibleText("Upcoming Session");
	
	//Entering Name
	CerotidWebsitePageElements.enterName(driver).sendKeys("Sanjaya" );
	
	//Entering Address
	CerotidWebsitePageElements.inputAddress(driver).sendKeys("123 Beltline rd" );
	
	//Entering city
		CerotidWebsitePageElements.inputCity(driver).sendKeys("Irving" );
		//Selecting state
		CerotidWebsitePageElements.selectState(driver).sendKeys("Tx" );
		//entering Zipcode
		CerotidWebsitePageElements.inputzip(driver).sendKeys("75038" );
		//entering email
		CerotidWebsitePageElements.inputemail(driver).sendKeys("sanjayakunwar333@gmail.com" );
		//entering phone
		CerotidWebsitePageElements.inputphone(driver).sendKeys("1234567890" );
		//entering visaStatus
		CerotidWebsitePageElements.selectVisaStatus(driver).sendKeys("Citizen" );
		//entering mediaSource
		CerotidWebsitePageElements.selectmediaSource(driver).sendKeys("Friends/Family" );
		//entering relocate
		CerotidWebsitePageElements.inputrelocate(driver).sendKeys("Yes" );
		//entering education
		CerotidWebsitePageElements.inputeducation(driver).sendKeys("Bachelors Running" );
		}
	
	
	
	
	
	
	
	
	
	

}

		
	

	
	

