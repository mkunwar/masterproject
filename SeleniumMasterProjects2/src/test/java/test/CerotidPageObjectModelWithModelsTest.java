package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.CerotidPageObjectModelWithMethods;
import pages.CerotidWebsitePageElements;

public class CerotidPageObjectModelWithModelsTest {

	// Class level variable
	public static WebDriver driver;

	public static void main(String[] args) {

		invokeBrowser();
		completeForm();

	}

	// 1: Invoke Browser
	public static void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.get("http://www.cerotid.com");
		driver.manage().window().maximize();

	}

	// Step2:Complete the form

	public static void completeForm() {

		// Create Object to access our page class

		CerotidPageObjectModelWithMethods obj = new CerotidPageObjectModelWithMethods(driver);

		obj.selectCourse("QA Automation");
		obj.selectSession("Upcoming Session");
		obj.enterName("Sanjaya Kunwar");
		
		
		


		
		
		

	}
}
