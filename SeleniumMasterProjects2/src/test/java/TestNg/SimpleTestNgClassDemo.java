package TestNg;

import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import pages.GoogleObject;

public class SimpleTestNgClassDemo {

	// Class level variable
	static WebDriver driver;

	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	public static void invokeBrowser() {
		driver.navigate().to("http://www.google.com");
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "---------------------------------was launched");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test(priority = 0)
	public static void performActions() {
		// navigate to google.com
		invokeBrowser();

		GoogleObject obj = new GoogleObject(driver);
		obj.enterSearchButton("what is TestNg");

	}

	@AfterTest
	public static void terminateTest() {
		driver.close();
	   driver.quit();
System.out.println("Terminated the Browser");
	}
}
