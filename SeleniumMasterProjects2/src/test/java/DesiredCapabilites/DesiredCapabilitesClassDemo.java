package DesiredCapabilites;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DesiredCapabilitesClassDemo {
	
	//1:How to invoke browser in incognito mode-hide cookies
	//2:Headless Mode - hidden browser
	//3: Ads removal - blocks all ads
	
	
	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		//Incognito
		
		/*ChromeOptions options = new ChromeOptions();
		options.addArguments("--incognito");
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY,options);
		
		WebDriver driver = new ChromeDriver(options);
		
		driver.get("https://www.guru99.com/smoke-testing.html#7");*/
		
		// Hidden Browser
		
		/*ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY,options);
		
		WebDriver driver = new ChromeDriver(options);
		
		driver.get("https://www.guru99.com/smoke-testing.html#7");
		System.out.println("Navigated to -----------------------" + driver.getTitle());*/
		
		//Ad blocker
		ChromeOptions options = new ChromeOptions();
		options.addExtensions(new File(".\\libs\\extension_4_6_0_0.crx"));
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		options.merge(capabilities);
		
		WebDriver driver = new ChromeDriver(options);
		
		driver.get("https://www.guru99.com/smoke-testing.html#7");
		
			
			
		
		
		

	}

}
