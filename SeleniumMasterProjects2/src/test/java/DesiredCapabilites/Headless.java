package DesiredCapabilites;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Headless {
	//Hidden Browser
	
public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
	ChromeOptions options = new ChromeOptions();
	options.addArguments("--headless");
	
	DesiredCapabilities capabilities = new DesiredCapabilities();
	capabilities.setCapability(ChromeOptions.CAPABILITY,options);
	
	WebDriver driver = new ChromeDriver(options);
	
	driver.get("https://www.google.com");
	System.out.println("Navigated to -----------------------" + driver.getTitle());
}
}
