package Extentreport;

import java.util.concurrent.TimeUnit;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import pages.GoogleObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class ExtentReportSimpleDemo {
	  static WebDriver driver;

	ExtentHtmlReporter htmlReporter;
	static ExtentReports extent;
	ExtentTest test;

	@BeforeSuite

	public void setUp() {
		// creating ExtentReporter object and creating new extentReport Html file
		// creating ExtentReports and attach the reporter(s)
		htmlReporter = new ExtentHtmlReporter("googleExtentReports.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}

	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		driver = new ChromeDriver();

	}

	public  void Sunil() {

		driver.navigate().to("https://www.google.com");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		if (driver.getTitle().contains("Google")) {
			System.out.println(driver.getTitle() + "-------------------was launched");
		} else {
			System.out.println("Fail Browser was not invoked----------------------");
			System.exit(0);
		}
		ExtentTest isBrowserOpened = extent.createTest("Sanjaya",
				"This websites ensures that the browser is invoked");
		isBrowserOpened.pass("Browser was invoked as Expected");
       
	
	}

	@Test
	public void navigateTOGoogle() {
		   Sunil();
		ExtentTest test = extent.createTest("Google Homepage", "This Page ensures it is launched");
		test.log(Status.INFO, "Homepage Launched");

		// creating a page object
		ExtentTest test1 = extent.createTest("Google page", "This page is launched");
		test1.log(Status.INFO, "Homepage launched");
		
		
		//creating page objects
	      GoogleObject obj = new GoogleObject(driver);
	      obj.enterSearchButton("What is TestNg");
	      test1.pass("enterSearchButton");
		obj.clickRadioButton();
		test1.pass("clickRadioButton");

	}

	@AfterSuite
	public void tearDown() {
		driver.close();
		driver.quit();
		extent.flush();
		ExtentTest isBrowserClosed = extent.createTest("Terminate Browser", "THE BROWSER IS BEING CLOSED");
		isBrowserClosed.pass("Browser is closed");
		

	}

}
