package ProgressivePages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {
	WebDriver driver = null;
	WebElement element = null;

	By clickAuto = By.xpath("(//span[contains(@class,'img')]//following::p[1])[1]");

	public HomePage(WebDriver driver) {
		this.driver = driver;

	}

	public void clickAuto() {
		driver.findElement(clickAuto).click();
	}

}


