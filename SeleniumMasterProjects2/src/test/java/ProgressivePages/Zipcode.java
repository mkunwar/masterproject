package ProgressivePages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Zipcode {
	WebDriver driver = null;
	WebElement element = null;

	By enterZipcode = By.xpath("//input[@id='zipCode_overlay']");
	By clickAuto = By.xpath("//input[@id='qsButton_overlay']");

	public Zipcode(WebDriver driver) {
		this.driver = driver;
	}

	public void enterZipcode(String Zipcode) {
		driver.findElement(enterZipcode).sendKeys(Zipcode);

	}

	public void clickAuto() {
		driver.findElement(clickAuto).click();
	}
}



