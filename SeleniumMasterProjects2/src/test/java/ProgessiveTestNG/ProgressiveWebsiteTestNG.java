package ProgessiveTestNG;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import ProgressivePages.AddVehicle;
import ProgressivePages.Driver1;
import ProgressivePages.HomePage;

public class ProgressiveWebsiteTestNG {
	static WebDriver driver;

	@BeforeSuite
	public void executeBeforeStartingTest() {

	}

	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		driver = new ChromeDriver();

	}

	public static void navigateTOProgressive() {

		driver.navigate().to("https://www.progressive.com");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		if (driver.getTitle().contains("Progressive")) {
			System.out.println(driver.getTitle() + "-------------------was launched");
		} else {
			System.out.println("Fail Browser was not invoked----------------------");
			System.exit(0);
		}

	}

	@Test(priority = 0)
	public static void Sanjaya() {
		navigateTOProgressive();

		HomePage obj = new HomePage(driver);
		obj.clickAuto();
	}

	@Test(priority = 1)
	public static void Zipcode() {

		ProgressivePages.Zipcode obj1 = new ProgressivePages.Zipcode(driver);
		obj1.enterZipcode("75038");
		obj1.clickAuto();
	}

	@Test(priority = 2)
	public static void PersonalInfo() throws InterruptedException {
		ProgressivePages.PersonalInfo obj2 = new ProgressivePages.PersonalInfo(driver);
		obj2.enterFirstName("sanjaya");
		obj2.enterMiddleName("");
		obj2.enterLastName("Kunwar");
		obj2.selectSuffix("Jr");
		obj2.enterDOB("04/10/1993");
		obj2.enterStreetNumberAndName("4031 N.Beltline Rd");
		obj2.enterApartment("");
		obj2.enterCity("");
		obj2.enterZipCode("");
		obj2.clickpostbox();
		obj2.clickQuote();
	}

	@Test(priority = 3)
	public static void ProgressiveAddVehicle() throws InterruptedException {
		AddVehicle obj3 = new AddVehicle(driver);
		obj3.clickVehicleYear("2018");
		obj3.clickVehicleMake("Dodge");
		obj3.clickVehicleModel("Challenger Hellcat");
		obj3.selectBodyType("2DR 6CYL");
		obj3.selectPrimaryUse("1");
		obj3.selectOwnorLease("2");
		obj3.selectVehicleOwnership("3");
		obj3.clickDone();
		obj3.clickContinue();
	}

	@Test(priority = 4)
	public static void ProgressiveDrivers() throws InterruptedException {
		Driver1 obj4 = new Driver1(driver);
		obj4.clickFemaleGender("");
		obj4.selectMaritalStatus("Single");
		obj4.selectEducationBackground("7");
		obj4.selectEmploymentStatus("7");
		obj4.enterSocialSecurity("123-45-6789");
		obj4.selectPrimaryResidence("7");
		obj4.selectMovedin("No");
		obj4.selectUSLicensedStatus("Valid");
		obj4.selectHistoryofDriving("7");
		obj4.clickClaims("No");
		obj4.clickTickets("No");
		obj4.clickContinue();

	}

	@AfterTest

	public static void terminateTest() {
		// driver.close();
		// driver.quit();
		System.out.println("Terminating the Browser");

	}

	@AfterMethod
	@AfterSuite
	public void tearDown() {

	}

}
